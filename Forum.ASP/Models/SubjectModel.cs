﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum.ASP.Models
{
    public class SubjectModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}