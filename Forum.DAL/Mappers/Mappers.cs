﻿using Forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Mappers
{
    internal static class Mappers
    {
        public static Subject ToSubject(IDataReader reader)
        {
            Subject result = new Subject();
            result.Id = (int)reader["Id"];
            result.Title = (byte[])reader["Title"];
            return result;
        }

        public static Response ToResponse(IDataReader reader)
        {
            Response result = new Response();
            result.Id = (int)reader["Id"];
            result.Date = (DateTime)reader["Date"];
            result.Author = (string)reader["Author"];
            result.Message = (string)reader["Message"];
            result.SubjectId = (int)reader["SubjectId"];
            return result;
        }
    }
}
